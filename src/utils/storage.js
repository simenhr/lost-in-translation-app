export const setStorage = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
}

export const getStorage = key => {
    const storedValue = localStorage.getItem(key);

    if (storedValue) {
        return JSON.parse(storedValue);
    }

    return false;
}

export const isTranslationLimit = () => {
   return getStorage('user_session').translated.length >= 10 || false;
}