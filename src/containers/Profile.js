import React from 'react';
import { useHistory } from 'react-router-dom';
import { getStorage } from '../utils/storage';
import withAuth from '../hoc/withAuth';
import './Profile.css';

const Profile = (props) => {


    const history = useHistory();
    const internalStorage = getStorage('user_session');

    const onGoTranslateClick = () => {
        history.replace('/translation');
    };

    const onLogoutClick = () => {
        localStorage.clear();
        props.onLogout();
    }

    const translateItem = (word) => {
        return (
            word.toLowerCase().split('')
                .map(letter => (letter + '.png'))
                .map(image => {
                    return (
                        <img src={image} alt="bilde" width="50" />
                    );
                }
                )
        )
    };

    const translateList = internalStorage.translated
        .map(word => {
            return (
                <div className="container" key={word}>
                    {translateItem(word)}
                    <p>Spells: {word}</p>
                </div>
            )
        }).reverse();

    return (
        <div>

            <header className="header-profile">
                <img src='Logo-Hello.png' alt="logo" width="175" />
                <h1>Lost in Translation </h1>
                <h4>Hello {getStorage('user_session').user}</h4>
            </header>

            <form>
                <button type="button" onClick={onGoTranslateClick}>Go to translation page</button>
                <button type="button" onClick={onLogoutClick}>Logout</button>
            </form>

            <div>
                <h2>Your last translations were:</h2>
                {translateList}
            </div>
        </div>
    )

}

export default withAuth(Profile);