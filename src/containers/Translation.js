import React, { useState, useEffect } from 'react';
import { getStorage, setStorage, isTranslationLimit } from '../utils/storage';
import { useHistory, Redirect } from 'react-router-dom';
import withAuth from '../hoc/withAuth';
import './Translation.css';


const Translation = (props) => {

    //let work = '';

    const history = useHistory();

    const [wordToTranslate, setWordToTranslate] = useState('');
    const [isLimitReached, setIsLimitReached] = useState(isTranslationLimit());
    const [newWord, setNewWord] = useState('');

    const internalStorage = getStorage('user_session');

    const onGoToProfileClick = () => {
        history.push('/profile');
    }

    const onWordToTranslateChange = e => setWordToTranslate(e.target.value.replace(/[^A-Za-z]/gi,"").trim());

    const onWordToTranslateClick = () => {
        let { user } = internalStorage;
        let oldTranslated = internalStorage.translated;
        updateStorage(user, oldTranslated);
        setNewWord(wordToTranslate);
    };

    const updateStorage = (user, oldTranslated) => {
        if (isTranslationLimit()) {
            oldTranslated.shift()
        }
        setStorage('user_session', {
            user,
            translated: [...oldTranslated, wordToTranslate]
        });
    }

    const translateItem = (word) => {
        return (
            word.toLowerCase().split('')
                .map(letter => (letter + '.png'))
                .map(image => {
                    return (
                        <img src={image} alt="bilde" width="50" />
                    );
                }
                )
        )
    };

    const translateList = internalStorage.translated
        .map(word => {
            return (
                <div className="container-translation" key={word}>
                    {translateItem(word)}
                    <p>Means: {word}</p>
                </div>
            )
        }).reverse()[0];

    return (
        <div>
            {!props.isLoggedIn && <Redirect to="/login" />}
            <header className="header-translation">
                <img src='Logo.png' alt="logo" width="150" />
                <h1>Lost in Translation </h1>
                <h4>Welcome {getStorage('user_session').user}!!</h4>
            </header>

            <form>
                <button type="button" onClick={onGoToProfileClick}>Go to profile</button>
            </form>

            <form>
                <div>
                    <p>What would you like to translate?</p>
                    <input type="text" placeholder="Type here" onChange={onWordToTranslateChange} />
                    <button type="button" onClick={onWordToTranslateClick}>Translate</button>
                </div>
            </form>
            <div>
                <h2>Translation:</h2>
                {translateList}
            </div>
        </div>
    )
}

export default withAuth(Translation);