import React, { useState, useEffect } from 'react';
import { setStorage, getStorage } from '../utils/storage';
import { Redirect } from 'react-router-dom';
import './Login.css';

const Login = (props) => {

    const [user, setUser] = useState('');

    const onUserChange = e => {
        setUser(e.target.value.trim());
    }

    const onUserCompleteClick = async () => {
        if (user.length === 0) {
            alert('Please Enter A Name')
            return;
        }
        setStorage('user_session', {
            user,
            translated: []
        }
        );
        props.onAuthenticated();
    }



    return (
        <div>

            {props.isLoggedIn && <Redirect to="/translation" />}
            <header className="login-header">
                <div>
                    <img src='Logo.png' alt="logo" width="150" />
                    <div className="header-title">
                        <h1>Lost In Translation</h1>
                        <h3>Let's Get Started!</h3>
                    </div>
                </div>

            </header>
            <form>
                <div>
                    <input type="text" placeholder="Enter your name here" onChange={onUserChange} />
                    <button type="button" onClick={onUserCompleteClick}>Login</button>

                </div>
            </form>

        </div>
    )


}

export default Login;