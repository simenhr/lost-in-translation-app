import React, { useEffect, useState } from 'react';
import './App.css';
import Login from './containers/Login';
import Translation from './containers/Translation';
import Profile from './containers/Profile';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import { getStorage } from './utils/storage';
import NotFound from './containers/NotFound';



function App() {

  const [isLoggedIn, setIsLoggedIn] = useState(getStorage('user_session'));

  console.log(isLoggedIn);

  const handleLogout = () => {
    setIsLoggedIn(false);
  }

  const handleAuthenticated = () => {
    setIsLoggedIn(true);
  }

  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/">
            <Redirect to="/login" />
          </Route>
          <Route path="/login"><Login isLoggedIn={isLoggedIn} onAuthenticated={handleAuthenticated} /></Route>
          <Route path="/translation"><Translation isLoggedIn={isLoggedIn} onAuthenticated={handleAuthenticated} /></Route>
          <Route path="/profile"><Profile isLoggedIn={isLoggedIn} onLogout={handleLogout} /></Route>
          <Route path="*" component={ NotFound } />
        </Switch>

      </div>
    </Router>

  );
}

export default App;
